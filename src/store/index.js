import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import {dummyData} from '@/store/dummyData.js'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    msg: 'Hello app',
    data: [],
    loading: false,
    loaded: false,
    error: null
  },
  getters: {
    msg: state => state.msg,
    getData: state => state.data,
    loading: state => state.loading,
    loaded: state => state.loaded,
    error: state => state.error
  },
  mutations: {
    setData (state, data) {
      state.data = data
    },
    toggleLoading (state) {
      state.loading = !state.loading
    },
    setLoaded (state, flag) {
      state.loaded = flag
    },
    setError (state, error) {
      state.error = error
    }
  },
  actions: {
    loadData({commit}) {
      commit('toggleLoading')
      axios.get('http://185.26.117.241:8081/pickup-data?page=0&size=1000')
        .then(response => {
          console.log(response)
          commit('setData', response.data._embedded.pickup_data_web)
          commit('toggleLoading')
          commit('setLoaded', true)
        })
        .catch(err =>{
          console.log(err)
          commit('toggleLoading')
          commit('setLoaded', false)
          commit('setError', err.message)
        })
    }
  }
})
