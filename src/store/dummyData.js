export const dummyData = [
  {
    datetime:'9/1/2014 0:01:00', 
    latitude: 40.2201, 
    longitude: -74.0021
  }, 
  
  {
    datetime:'9/1/2014 0:01:00', 
    latitude: 40.75, 
    longitude: -74.0027
  }, 
  
  {
    datetime:'9/1/2014 0:03:00', 
    latitude: 40.7559, 
    longitude: -73.9864
  },
  
  {
    datetime:'9/1/2014 0:06:00',
    latitude: 40.745, 
    longitude: -73.9889
  },
  
  {
    datetime:'9/1/2014 0:11:00', 
    latitude: 40.8145, 
    longitude: -73.9444
  },
  
  {
    datetime:'9/1/2014 0:12:00', 
    latitude: 40.6735, 
    longitude: -73.9918
  },
  
  {
    datetime:'9/1/2014 0:15:00', 
    latitude: 40.7471, 
    longitude: -73.6472
  },
  
  {
    datetime:'9/1/2014 0:16:00', 
    latitude: 40.6613, 
    longitude: -74.2691
  },
  
  {
    datetime:'9/1/2014 0:32:00', 
    latitude: 40.3745, 
    longitude: -73.9999
  },
  
  {
    datetime:'9/1/2014 0:33:00', 
    latitude: 40.7633, 
    longitude: -73.9773
  },
  
  {
    datetime:'9/1/2014 0:33:00', 
    latitude: 40.7467, 
    longitude: -73.6131
  },
  
  {
    datetime:'9/1/2014 0:37:00', 
    latitude: 40.8105, 
    longitude: -73.96}
    
    
    ,
    {datetime:'9/1/2014 0:38:00',
    latitude: 40.679, 
    longitude: -74.0111
  },
  
  {
    datetime:'9/1/2014 0:39:00', 
    latitude: 40.4023, 
    longitude: -73.9839
  },
  
  {
    datetime:'9/1/2014 0:48:00', 
    latitude: 40.7378, 
    longitude: -74.0395
  },
  
  {
    datetime:'9/1/2014 0:48:00', 
    latitude: 40.7214, 
    longitude: -73.9884
  },
  
  {
    datetime:'9/1/2014 0:49:00', 
    latitude: 40.8646, 
    longitude: -73.9081
  },
  
  {
    datetime:'9/1/2014 1:08:00', 
    latitude: 40.7398, 
    longitude: -74.0061
  },
  
  {
    datetime:'9/1/2014 1:17:00', 
    latitude: 40.6793, 
    longitude: -74.0116
  },
  
  {
    datetime:'9/1/2014 1:19:00', 
    latitude: 40.7328, 
    longitude: -73.9875
  },
  
  {
    datetime:'9/1/2014 1:39:00', 
    latitude: 40.6743, 
    longitude: -73.9334
  },
  
  {
    datetime:'9/1/2014 1:41:00', 
    latitude: 40.7638, 
    longitude: -73.9962
  },
  
  {
    datetime:'9/1/2014 2:03:00', 
    latitude: 40.7638, 
    longitude: -73.9811
  },
  
  {
    datetime:'9/1/2014 2:13:00', 
    latitude: 40.7415, 
    longitude: -73.9975
  },
  
  {
    datetime:'9/1/2014 2:33:00', 
    latitude: 40.7445, 
    longitude: -73.9855
  },
  
  {
    datetime:'9/1/2014 2:39:00', 
    latitude: 40.6614, 
    longitude: -73.9409
  },
  
  {
    datetime:'9/1/2014 2:41:00', 
    latitude: 40.7494, 
    longitude: -73.9946
  },
  
  {
    datetime:'9/1/2014 3:18:00', 
    latitude: 40.7529, 
    longitude: -74.004
  },
  {
    datetime:'9/1/2014 4:01:00', 
    latitude: 41.0319, 
    longitude: -74.1813
  },
  
  {
    datetime:'9/1/2014 4:06:00', 
    latitude: 40.7983, 
    longitude: -73.9628
  },
  
  {
    datetime:'9/1/2014 4:20:00', 
    latitude: 40.6932, 
    longitude: -73.943}
    
    ,
    
    {datetime:'9/1/2014 4:34:00',
    latitude: 40.707, 
    longitude: -74.0045
  },
  
  {
    datetime:'9/1/2014 5:02:00', 
    latitude: 40.7193, 
    longitude: -73.9998
  },
  
  {
    datetime:'9/1/2014 5:11:00', 
    latitude: 40.7608, 
    longitude: -73.9859
  },
  
  {
    datetime:'9/1/2014 5:12:00', 
    latitude: 40.8293, 
    longitude: -73.9432
  },
  
  {
    datetime:'9/1/2014 5:16:00', 
    latitude: 40.7612, 
    longitude: -73.9874
  },
  
  {
    datetime:'9/1/2014 5:25:00', 
    latitude: 40.7215, 
    longitude: -73.9841
  },
  
  {
    datetime:'9/1/2014 5:29:00', 
    latitude: 40.7778, 
    longitude: -73.9888
  },
  
  {
    datetime:'9/1/2014 5:45:00', 
    latitude: 40.7501, 
    longitude: -74.0025
  },
  
  {
    datetime:'9/1/2014 5:55:00', 
    latitude: 40.7769, 
    longitude: -73.9862
  },
  
  {
    datetime:'9/1/2014 5:56:00', 
    latitude: 40.7499, 
    longitude: -73.9717
  },
  
  {
    datetime:'9/1/2014 6:04:00', 
    latitude: 40.7513, 
    longitude: -73.935
  },
  {
    datetime:'9/1/2014 6:08:00', 
    latitude: 40.7291, 
    longitude: -73.9813
  },
  
  {
    datetime:'9/1/2014 6:14:00', 
    latitude: 40.7674, 
    longitude: -73.9841
  },
  
  {
    datetime:'9/1/2014 6:26:00', 
    latitude: 40.7414, 
    longitude: -73.9983
  },
  
  {
    datetime:'9/1/2014 6:35:00', 
    latitude: 40.7685, 
    longitude: -73.8626
  },
  
  {
    datetime:'9/1/2014 6:43:00', 
    latitude: 40.8298, 
    longitude: -73.9719
  },
  
  {
    datetime:'9/1/2014 6:51:00', 
    latitude: 40.6878, 
    longitude: -73.9562
  },
  
  {
    datetime:'9/1/2014 6:56:00', 
    latitude: 40.7686, 
    longitude: -73.8626
  },
  
  {
    datetime:'9/1/2014 7:01:00', 
    latitude: 40.7878, 
    longitude: -73.5375
  },
  
  {
    datetime:'9/1/2014 7:22:00', 
    latitude: 40.7636, 
    longitude: -73.9911
  },
  
  {
    datetime:'9/1/2014 7:30:00', 
    latitude: 40.7364, 
    longitude: -73.9966
  },
  
  {
    datetime:'9/1/2014 7:33:00', 
    latitude: 40.7593, 
    longitude: -73.9752
  },
  
  {
    datetime:'9/1/2014 7:37:00', 
    latitude: 40.7955, 
    longitude: -74.2537
  },
  
  {
    datetime:'9/1/2014 7:37:00', 
    latitude: 40.7303, 
    longitude: -73.9998
  },
  
  
  {
    datetime:'9/1/2014 7:41:00',
    latitude: 40.756, 
    longitude: -73.9728
  },
  
  {
    datetime:'9/1/2014 7:58:00', 
    latitude: 40.7558, 
    longitude: -73.9764
  },
  
  {
    datetime:'9/1/2014 8:05:00', 
    latitude: 40.7739, 
    longitude: -73.8725
  },
  
  {
    datetime:'9/1/2014 8:11:00', 
    latitude: 40.6788, 
    longitude: -73.9586
  },
  
  {
    datetime:'9/1/2014 8:13:00', 
    latitude: 40.6897, 
    longitude: -73.9724
  },
  
  {
    datetime:'9/1/2014 8:19:00', 
    latitude: 40.7359, 
    longitude: -73.9838
  },
  
  {
    datetime:'9/1/2014 8:21:00', 
    latitude: 40.7312, 
    longitude: -74.0047
  },
  
  {
    datetime:'9/1/2014 8:25:00', 
    latitude: 40.7255, 
    longitude: -74.01
  }
  
  
  ,{
    datetime:'9/1/2014 8:30:00', 
    latitude: 40.7344, 
    longitude: -73.9859
  },
  
  {
    datetime:'9/1/2014 8:30:00', 
    latitude: 40.7878, 
    longitude: -73.9537
  },
  
  {
    datetime:'9/1/2014 8:34:00', 
    latitude: 40.7666, 
    longitude: -73.9903
  },
  
  
  {
    datetime:'9/1/2014 8:37:00',
    latitude: 40.751, 
    longitude: -74.0337
  },
  
  
  {
    datetime:'9/1/2014 8:40:00',
    latitude: 40.764, 
    longitude: -73.9793
  },
  
  {
    datetime:'9/1/2014 8:44:00', 
    latitude: 40.7173, 
    longitude: -73.9499
  },
  
  
  {
    datetime:'9/1/2014 8:45:00',
    latitude: 40.763, 
    longitude: -73.9597
  },
  
  {
    datetime:'9/1/2014 8:53:00', 
    latitude: 40.7553, 
    longitude: -73.973
  },
  {
    datetime:'9/1/2014 8:53:00', 
    latitude: 40.6874, 
    longitude: -73.998
  },
  {
    datetime:'9/1/2014 8:57:00', 
    latitude: 40.8205, 
    longitude: -73.9541
  },
  
  {
    datetime:'9/1/2014 8:57:00', 
    latitude: 40.7459, 
    longitude: -73.9883
  },
  
  {
    datetime:'9/1/2014 8:57:00', 
    latitude: 40.7798, 
    longitude: -74.0089
  },
  
  
  {
    datetime:'9/1/2014 8:59:00',
    latitude: 40.695, 
    longitude: -74.178
  },
  
  {
    datetime:'9/1/2014 8:59:00',
    latitude: 40.695, 
    longitude: -74.178
  },
  {
    datetime:'9/1/2014 9:04:00', 
    latitude: 40.7375, 
    longitude: -73.9799
  },
  
  
  {
    datetime:'9/1/2014 9:04:00',
    latitude: 40.722, 
    longitude: -73.9882
  },
  
  {
    datetime:'9/1/2014 9:05:00', 
    latitude: 40.7277, 
    longitude: -73.9895
  },
  
  {
    datetime:'9/1/2014 9:05:00', 
    latitude: 40.7457, 
    longitude: -74.0261
  },
  
  {
    datetime:'9/1/2014 9:10:00', 
    latitude: 40.8449, 
    longitude: -73.9371
  },
  
  {
    datetime:'9/1/2014 9:10:00', 
    latitude: 40.5896, 
    longitude: -73.7384
  },
  
  {
    datetime:'9/1/2014 9:12:00', 
    latitude: 40.7644, 
    longitude: -73.9958
  },
  
  {
    datetime:'9/1/2014 9:13:00', 
    latitude: 40.7275, 
    longitude: -74.0386
  },
  
  {
    datetime:'9/1/2014 9:14:00', 
    latitude: 40.7556, 
    longitude: -73.9818
  },
  
  {
    datetime:'9/1/2014 9:15:00', 
    latitude: 40.7215, 
    longitude: -74.0045
  },
  
  {
    datetime:'9/1/2014 9:16:00', 
    latitude: 40.6917, 
    longitude: -73.9425
  },
  
  {
    datetime:'9/1/2014 9:17:00', 
    latitude: 40.7557, 
    longitude: -73.9923
  },
  
  {
    datetime:'9/1/2014 9:21:00', 
    latitude: 40.7202, 
    longitude: -73.9962
  },
  
  {
    datetime:'9/1/2014 9:22:00', 
    latitude: 40.7324, 
    longitude: -73.9971
  },
  {
    datetime: '9/2/2014 3:36:00',
    latitude:	40.6687,
    longitude:	-73.7247
  }, 
  {
    datetime: '9/2/2014 3:47:00',
    latitude: 40.7648,
    longitude:	-73.9882
  }
]