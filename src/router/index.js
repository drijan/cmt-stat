import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/components/Dashboard'
import MapDash from '@/components/MapDash'
import store from '@/store'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'MapDash',
      component: MapDash,
      beforeEnter (to, from, next) {
        store.dispatch('loadData')
        next()
      }
    },
    {
      path: '/dash',
      name: 'Dashboard',
      component: Dashboard,
      beforeEnter (to, from, next) {
        store.dispatch('loadData')
        next()
      }
    }
  ]
})
